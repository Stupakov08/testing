﻿using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testlab5_6.pageObjects;

namespace testlab5_6
{
    class Pages
    {
        private static T getPages<T>() where T : new()
        {
            var page = new T();
            PageFactory.InitElements(Base.WebDriver, page);
            return page;
        }

        public static RunPassGenPage RunPassPage => getPages<RunPassGenPage>();
        public static ExtendedRunPassGenPage ExtendedRunPassPage => getPages<ExtendedRunPassGenPage>();


    }
}
