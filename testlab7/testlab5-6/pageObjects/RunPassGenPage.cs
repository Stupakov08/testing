﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testlab5_6.pageObjects
{
    public class RunPassGenPage : AbstractPage
    {
        public RunPassGenPage()
        {
          
        }
        public string pageUrl = "https://www.random.org/passwords/";

        [FindsBy(How = How.CssSelector, Using = "input[value='Get Passwords']")]
        private IWebElement submit;
        [FindsBy(How = How.Name, Using = "num")]
        private IWebElement num;
        [FindsBy(How = How.Name, Using = "len")]
        private IWebElement len;

        public void GoToUrl()
        {
            WebDriver.Navigate().GoToUrl(pageUrl);
        }
        public RunPassGenPage Submit()
        {
            submit.Click();
            return this;
        }
        public RunPassGenPage EnterNumber(string number)
        {
            num.Click();
            num.Clear();
            num.SendKeys(number);
            return this;
        }
        public RunPassGenPage EnterLenght(string lenght)
        {
            len.Click();
            len.Clear();
            len.SendKeys(lenght);
            return this;
        }
        public IWebElement GetData()
        {
            return WebDriver.FindElement(By.ClassName("data"));
        }
        public ICollection<IWebElement> GetPasswords()
        {
            return WebDriver.FindElements(By.CssSelector(".data > li"));
        }
    }
}
