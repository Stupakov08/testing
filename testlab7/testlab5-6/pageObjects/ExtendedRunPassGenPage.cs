﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testlab5_6.pageObjects
{
    class ExtendedRunPassGenPage : RunPassGenPage
    {
        public ExtendedRunPassGenPage() { }
        [FindsBy(How = How.CssSelector, Using = "input[value='Switch to Advanced Mode']")]
        private IWebElement advanced;
        [FindsBy(How = How.CssSelector, Using = "input[name='rnd']")]
        private IList<IWebElement> radio;
        [FindsBy(How = How.Id, Using = "id")]
        private IWebElement id;

        public ExtendedRunPassGenPage EnterId(string ident)
        {
            advanced.Click();
            radio.Last().Click();
            id.Click();
            id.Clear();
            id.SendKeys(ident);
            return this;
        }
    }
}
