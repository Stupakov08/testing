﻿using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testlab5_6
{
    class Base
    {
        public static ChromeDriver WebDriver;
        public static void InitDriver()
        {
            WebDriver = new ChromeDriver(ConfigurationManager.AppSettings["driverPath"]);
        }
        public static void Quit()
        {
            WebDriver.Quit();
        }
    }
}
