﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testlab5_6
{
    [TestFixture]
    public class CheckPasswordGeneration
    {
        private StringBuilder verificationErrors;
        private string baseURL;
        private bool acceptNextAlert = true;
        private string passCount = ConfigurationManager.AppSettings["passCount"];
        private string passLength = ConfigurationManager.AppSettings["passLenght"];


        [SetUp]
        public void SetupTest()
        {   
            Base.InitDriver();
            baseURL = "https://www.katalon.com/";
            verificationErrors = new StringBuilder();
        }

        [TearDown]
        public void TeardownTest()
        {
            try
            {
                Base.Quit();
            }
            catch (Exception)
            {
                // Ignore errors if unable to close the browser
            }
            Assert.AreEqual("", verificationErrors.ToString());
        }

        [Test]
        public void TheCheckPasswordGeneration()
        {
            var RunPassPage = Pages.RunPassPage;
            RunPassPage.GoToUrl();
            RunPassPage.Submit();
            var response = RunPassPage.GetData();
            PasswordExistAssertion(response);
        }
        [Test]
        public void TheNumberOfGeneratedPasswords()
        {
            var RunPassPage = Pages.RunPassPage;
            RunPassPage.GoToUrl();
            RunPassPage.EnterNumber(passCount).Submit();
            var response = RunPassPage.GetPasswords();
            PasswordNumberAssertion(response);
        }
        [Test]
        public void TheLengthOfGeneratedPasswords()
        {
            var RunPassPage = Pages.RunPassPage;
            RunPassPage.GoToUrl();
            RunPassPage.EnterLenght(passLength).Submit();
            var response = RunPassPage.GetPasswords();
            PasswordLenghtAssertion(response);
        }
        private void PasswordExistAssertion(IWebElement response)
        {
            try
            {
                Assert.IsTrue(response.Text.Length > 0);
            }
            catch (Exception e)
            {
                verificationErrors.Append("Passwords were not generated");
            }
        }
        private void PasswordNumberAssertion(ICollection<IWebElement> response)
        {
            try
            {
                Assert.IsTrue(response.Count.ToString() == passCount);
            }
            catch (Exception e)
            {
                verificationErrors.Append("Password Count is not correct");
            }
        }
        private void PasswordLenghtAssertion(ICollection<IWebElement> response)
        {
            try
            {
                Assert.IsTrue(response.First().Text.Length.ToString() == passLength);
            }
            catch (Exception e)
            {
                verificationErrors.Append("Password Count is not correct" + response.First().Text.Length.ToString());
            }
        }


    }
}
